'use strict';

/* Services */


// Demonstrate how to register services
// In this case it is a simple value service.
var mbtiMatrixServices = angular.module('mbtiMatrix.services', ['ngResource']);

mbtiMatrixServices.value('version', '0.1');
mbtiMatrixServices.factory('Type',
			   ['$resource',
			    function($resource){
				return $resource('types/:type.json', {}, {
				    query: {method:'GET',
					    params:{type:'types'},
					    isArray:true}
				});
			    }]);
