'use strict';

/* Controllers */

angular.module('mbtiMatrix.controllers', []).
  controller('matrixCtrl', ['$scope', 'Type', function($scope, Type) {
      $scope.types = Type.query();
  }])
  .controller('functionCtrl', [function() {

  }])
  .controller('typeCtrl', [function() {

  }]);
