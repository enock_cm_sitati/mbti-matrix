'use strict';


// Declare app level module which depends on filters, and services
angular.module('mbtiMatrix', [
  'ngRoute',
  'mbtiMatrix.filters',
  'mbtiMatrix.services',
  'mbtiMatrix.directives',
  'mbtiMatrix.controllers'
]).
config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/matrix', {templateUrl: 'partials/matrix.html', controller: 'matrixCtrl'});
  $routeProvider.when('/function/:function', {templateUrl: 'partials/function.html', controller: 'functionCtrl'});
  $routeProvider.when('/type/:type', {templateUrl: 'partials/type.html', controller: 'typeCtrl'});
  $routeProvider.otherwise({redirectTo: '/matrix'});
}]);
