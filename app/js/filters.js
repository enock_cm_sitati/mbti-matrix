'use strict';

/* Filters */

angular.module('mbtiMatrix.filters', []).
  filter('interpolate', ['version', function(version) {
    return function(text) {
      return String(text).replace(/\%VERSION\%/mg, version);
    }
  }]);
angular.module('mbtiMatrix.filters', []).
    filter('vertical', function() {
	return function(input) {
	    return input.split("").join("&lt;br&gt;")
	};
    });
